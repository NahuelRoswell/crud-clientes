package springboot.backend.apirest.models.dao;

import org.springframework.data.repository.CrudRepository;
import springboot.backend.apirest.models.entity.Client;

public interface IClientDAO extends CrudRepository<Client, Long> {
}
