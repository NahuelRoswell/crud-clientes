package springboot.backend.apirest.models.services;

import springboot.backend.apirest.models.entity.Client;

import java.util.List;

public interface IClientService {
    List<Client> findAll();

    Client findById(Long id);

    Client save(Client client);

    void delete(Long id);
}
