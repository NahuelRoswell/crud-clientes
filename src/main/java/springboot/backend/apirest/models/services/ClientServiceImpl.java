package springboot.backend.apirest.models.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.backend.apirest.models.dao.IClientDAO;
import springboot.backend.apirest.models.entity.Client;

import java.util.List;

@Service
public class ClientServiceImpl implements IClientService{
    @Autowired
    private IClientDAO clientDAO;

    @Override
    @Transactional(readOnly = true)
    public List<Client> findAll() {
        return (List<Client>) clientDAO.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Client findById(Long id) {
        return clientDAO.findById(id).orElse(null);
    }

    @Override
    public Client save(Client client) {
        return clientDAO.save(client);
    }

    @Override
    public void delete(Long id) {
        clientDAO.deleteById(id);
    }
}
